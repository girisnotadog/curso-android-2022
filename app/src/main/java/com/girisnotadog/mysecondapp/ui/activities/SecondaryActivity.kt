package com.girisnotadog.mysecondapp.ui.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.girisnotadog.mysecondapp.R
import com.girisnotadog.mysecondapp.databinding.ActivitySecondaryBinding
import com.girisnotadog.mysecondapp.ui.fragments.FragmentOne
import com.girisnotadog.mysecondapp.ui.fragments.FragmentTwo

class SecondaryActivity : AppCompatActivity() {
    private val TAG: String = javaClass.simpleName

    private lateinit var binding: ActivitySecondaryBinding
    private val fragmentManager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Que renderizar
        //setContentView(R.layout.activity_secondary)

        binding = ActivitySecondaryBinding.inflate( layoutInflater )
        setContentView( binding.root )

//        enlazarFindViewById()

        enlazarBinding()

        Log.d(TAG, "Evento onCreate")
    }

    fun enlazarBinding() {
        binding.activitySecondaryTextviewTitle.text = "Hola mundo binding"

        binding.activitySecondaryButtonDemo.setOnClickListener {
            binding.activitySecondaryTextviewTitle.text = "Me presionaste desde el Binding"

            openMainAct()
        }
        val fragmentOne = FragmentOne.newInstance("https://cdnb.artstation.com/p/assets/images/images/048/977/271/large/sanal-ts-picsart-22-02-26-20-33-22-232.jpg?1651409916", "Reptiliana")
        val fragmentTwo = FragmentTwo.newInstance()


        fragmentManager.beginTransaction()
            .add( binding.activitySecondaryViewContainer.id, fragmentOne, "fragment1")
            .addToBackStack("fragment1")
            .commit()

        fragmentManager.beginTransaction()
            .replace( binding.activitySecondaryViewContainer.id, fragmentTwo, "fragment2")
            .addToBackStack("fragment2")
//            .add( binding.activitySecondaryViewContainer.id, fragmentOne)
//            .add( binding.activitySecondaryViewContainer.id, fragmentOne)
            .commit()
    }

    fun enlazarFindViewById() {
        val tv: TextView = findViewById( R.id.activity_secondary_textview_title )
        tv.text = "Hola mundo"

        val btn: Button = findViewById( R.id.activity_secondary_button_demo )
        btn.setOnClickListener {
            tv.text = "Ohh no, me has presionado muy duro. Yamete !!"

            openMainAct()
        }
    }

    fun openMainAct() {
        val myIntent = Intent()

        // Definimos de forma explicita el Componente del Intent
        myIntent.setClass( applicationContext, MainActivity::class.java )

        // Forma implicita de llamar a un Intent
        myIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

//        myIntent.action = Intent.ACTION_MAIN
////        myIntent.setData(android.net.Uri.parse("http://www.google.com"))
//        myIntent.addCategory( Intent.CATEGORY_APP_MESSAGING )

        startActivity( myIntent )
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "Evento onStart")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "Evento onRestart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "Evento onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "Evento onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "Evento onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Evento onDestroy")
    }

    override fun onBackPressed() {
        if ( fragmentManager.backStackEntryCount > 0 )
            fragmentManager.popBackStack()
        else
            super.onBackPressed()

        Log.d(TAG, "Evento onBackPressed")
    }
}