package com.girisnotadog.mysecondapp.ui.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.girisnotadog.mysecondapp.MyApplication
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.database.MyAppDatabase
import com.girisnotadog.mysecondapp.data.datastore.mySettings
import com.girisnotadog.mysecondapp.data.dto.LoginRequestDto
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.data.entities.User
import com.girisnotadog.mysecondapp.repositories.AuthRepository
import com.girisnotadog.mysecondapp.repositories.MemeRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainActivityViewModel(application: Application): AndroidViewModel(application) {

    // Obtenemos la instancia de la base de datos
    private val myAppDatabase = MyAppDatabase.getInstance( application.applicationContext )
    private val authRespository = AuthRepository( application.applicationContext )

    // Pasamos el DAO de la base de datos al repositorio
    private val memeRepository = MemeRepository( myAppDatabase.memesDao() )

    // Contenedor de estados
    // Persisitir la información o el estado de lo que se muestra en la interfaz gráfica

    // 1 Elemento interno modificable
    private val _listaMemes = MutableStateFlow<Resource<List<Meme>>> ( Resource.loading() )

    // 2 Elemento externo consumible y no modificable
    val listaMemes: StateFlow<Resource<List<Meme>>> = _listaMemes


    private val _userForLogin = MutableStateFlow<User>( User("", "", "", 0) )
    val userForLogin: StateFlow<User> = _userForLogin

    private val _loginTry = MutableStateFlow<Resource<Boolean>>( Resource.standBy( false ) )
    val loginTry: StateFlow<Resource<Boolean>> = _loginTry


    fun saveLoginForm( user: String, password: String ) {

    }

    /**
     * Intenta iniciar sesión
     * Si es exitoso, guarda el token y notifica que fue exitoso
     * Si es fracasado, muestra el mensaje de error
     */
    fun login( user: String, password: String) {
        viewModelScope.launch {
            authRespository.doLogin(
                LoginRequestDto( user, password )
            ).collect {
                _loginTry.value = it
            }
        }
    }

    val myConfig = application.applicationContext.mySettings

    fun updateList() {
        viewModelScope.launch {
            _listaMemes.value = Resource.loading()

            try {
                memeRepository.updateMemes()
            } catch (e: Exception) {
                _listaMemes.value = Resource.error("Error al actualizar", e)
            }
        }
    }

    init {
        // Corrutina del ViewModel
        viewModelScope.launch {

            memeRepository.getMemes().collect {
                _listaMemes.value = it
            }

            Log.d("MainActivityViewModel", "ViewModel iniciado antes de corutina 2")

            delay(1000)

            Log.d("MainActivityViewModel", "ViewModel iniciado en corutina 3")
        }
        Log.d("MainActivityViewModel", "ViewModel iniciado fuera de la corutina 1")
    }
}