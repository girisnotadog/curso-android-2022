package com.girisnotadog.mysecondapp.ui.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavController

@Composable
fun RandomLayout(navController: NavController, randomText: String) {
    Column() {
        Text("Texto random: $randomText")

        Button(onClick = { navController.navigate("dashboard") }) {
            Text(text = "Dashboard")
        }
    }
}