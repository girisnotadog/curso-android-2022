package com.girisnotadog.mysecondapp.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.girisnotadog.mysecondapp.R
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.databinding.FragmentTwoBinding
import com.girisnotadog.mysecondapp.ui.adapters.RVMemesAdapter
import com.girisnotadog.mysecondapp.ui.viewmodels.FragmentTwoViewModel
import kotlinx.coroutines.launch

class FragmentTwo : Fragment() {
    private lateinit var viewModel: FragmentTwoViewModel
    private lateinit var binding : FragmentTwoBinding

    companion object {
        fun newInstance() = FragmentTwo()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTwoBinding.inflate( inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FragmentTwoViewModel::class.java)

        loadMemeList()
    }

    fun loadMemeList() {
        val layoutManager = LinearLayoutManager( requireContext(), LinearLayoutManager.VERTICAL, false)
        val adapter = RVMemesAdapter( listOf() ) {
            Toast.makeText( requireContext(), "Hiciste click en el meme: ${it.id}", Toast.LENGTH_SHORT).show()
        }

        lifecycleScope.launch {
            viewModel.memesFlow.collect {
                binding.fragmentTwoSwipeRefresh.isRefreshing = false

                when( it ) {
                    is Resource.Error -> Toast.makeText( requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    is Resource.Loading -> binding.fragmentTwoSwipeRefresh.isRefreshing = true
                    is Resource.Success -> {
                        adapter.memeList = it.data
                        binding.fragmentTwoRvMemes.adapter?.notifyDataSetChanged()
                    }
                    else -> {}
                }
            }
        }

        binding.fragmentTwoRvMemes.layoutManager = layoutManager
        binding.fragmentTwoRvMemes.adapter = adapter

        binding.fragmentTwoSwipeRefresh.setOnRefreshListener {
            viewModel.reload()
        }
    }
}