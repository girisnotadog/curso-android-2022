package com.girisnotadog.mysecondapp.ui.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.ui.components.Lista
import kotlinx.coroutines.flow.Flow
import java.util.*

@Composable
fun ListaMemesLayout(navController: NavController, listaMemes: Flow<Resource<List<Meme>>>, update: () -> Unit) {
    val lista = remember {
        mutableStateOf<List<Meme>>(listOf())
    }

    val error = remember {
        mutableStateOf("")
    }

    val cargando = remember {
        mutableStateOf(false)
    }

    if (cargando.value) {
        Text(text = "Cargando la información...")
        Spacer(modifier = Modifier.height(10.dp))
    }

    if (error.value.isNotEmpty()) {
        Text(text = error.value)
        Spacer(modifier = Modifier.height(10.dp))
    }

    LaunchedEffect(key1 = Unit) {
        listaMemes.collect { recurso ->
            cargando.value = false

            when (recurso) {
                is Resource.Error -> {
                    error.value = "${recurso.message} ${recurso.exception?.localizedMessage}"
                }
                is Resource.Loading -> {
                    cargando.value = true
                    error.value = ""
                }
                is Resource.Success -> {
                    lista.value = recurso.data
                }
                else -> {}
            }
        }
    }

    val uuid by remember {
        mutableStateOf("${UUID.randomUUID()}")
    }

    Column() {
        Button(onClick = {
            navController.navigate("random/$uuid}") {
                launchSingleTop = true
            }

        }) {
            Text("Random igual")
        }

        Spacer(modifier = Modifier.height(10.dp))
        Lista(memes = lista.value, update) { wich: Long, howMuch: Int -> }
    }
}