package com.girisnotadog.mysecondapp.ui.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.datastore.core.DataStore
import androidx.navigation.NavController
import com.girisnotadog.mysecondapp.data.datastore.MySettingsStore
import com.girisnotadog.mysecondapp.data.entities.User
import kotlinx.coroutines.launch
import java.util.*

@Composable
fun UserLayout(navController: NavController, config: DataStore<MySettingsStore>) {
    // Actualmente guardado en DS
    var user by remember {
        mutableStateOf( User("NA", "NA", "", 0) )
    }

    // Contener el estado de los edits
    var userEdit by remember { mutableStateOf( "xx" ) }
    var emailEdit by remember { mutableStateOf( "xx" ) }
    var ageEdit by remember { mutableStateOf( "xxx" ) }

    // Corrutina para guardar la información en datastore
    val coroutineScope = rememberCoroutineScope()

    // Enlazar la vista con el Flow de DS
    LaunchedEffect(key1 = Unit) {
        config.data.collect {
            userEdit = it.name
            emailEdit = it.email
            ageEdit = it.age.toString()

            user = User( it.name, it.email, "", it.age )
        }
    }

    Column {
        Button(onClick = {
            navController.navigate("random/${UUID.randomUUID()}")
        }) {
            Text("Random nuevo")
        }

        Spacer(modifier = Modifier.height(10.dp))

        // Muestra lo que está actualmente en DS
        Text(text = "Nombre: ${user.name}")
        Text(text = "Email: ${user.email}")
        Text(text = "Age: ${user.age}")

        Spacer(modifier = Modifier.height(10.dp))

        TextField(value = userEdit, onValueChange = { userEdit = it })
        TextField(value = emailEdit, onValueChange = { emailEdit = it })
        TextField(value = ageEdit, onValueChange = { ageEdit = it })

        Button(onClick = {
            coroutineScope.launch {
                // Actualizamos el DS con un builder
                config.updateData {
                    it.toBuilder().apply {
                        name = userEdit
                        email = emailEdit
                        age = ageEdit.toInt()
                    }
                    .build()
                }
            }
        }) {
            Text(text = "Guardar")
        }
    }
}