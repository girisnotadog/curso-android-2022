package com.girisnotadog.mysecondapp.ui.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.ui.viewmodels.MainActivityViewModel
import kotlinx.coroutines.launch
import androidx.fragment.app.activityViewModels
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.girisnotadog.mysecondapp.ui.layouts.*

class MainActivity : ComponentActivity() {
    private val mainActivityViewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val navController = rememberNavController()

            NavHost(navController = navController, startDestination = "login") {
                composable("user") {
                    UserLayout( navController, mainActivityViewModel.myConfig )
                }

                composable("memes") {
                    ListaMemesLayout( navController, mainActivityViewModel.listaMemes ) {
                        mainActivityViewModel.updateList()
                    }
                }

                composable("random/{textoRandom}") {
                    RandomLayout(navController = navController, randomText = it.arguments?.getString("textoRandom") ?: "")
                }

                composable("dashboard") {
                    DashboardLayout(navController = navController)
                }

                composable("login") {
                    LoginLayout(navController = navController, mainActivityViewModel.loginTry ) { user, pass ->
                        mainActivityViewModel.login( user, pass )
                    }
                }


            }

        }
    }
}