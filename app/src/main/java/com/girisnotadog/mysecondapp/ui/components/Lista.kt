package com.girisnotadog.mysecondapp.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.girisnotadog.mysecondapp.R
import com.girisnotadog.mysecondapp.data.entities.Meme

@Composable
// fun Lista( memes: List<Meme>, callback: MemeListaInterface ) {
fun     Lista(memes: List<Meme>, update: () -> Unit, callback: (wich: Long, howMuch: Int) -> Unit) {
    Column() {
        Text(text = "Actualizar!!", Modifier.clickable {
            update()
        })

        Spacer(modifier = Modifier.height(20.dp))

        LazyColumn() {
            items(memes) {
                ElementoLista( it, callback)
            }
        }
    }
}

@Composable
//fun ElementoLista( meme: Meme, callback: MemeListaInterface ) {
fun ElementoLista( meme: Meme, callback: (wich: Long, howMuch: Int) -> Unit ) {
    Row() {
        Image(
            painter = painterResource(R.drawable.ic_meme_dummy_24),
            contentDescription = meme.name,
            modifier = Modifier.size(40.dp)
        )
    }
    Row() {
        Text(text = meme.name)
    }
    Row() {
        repeat( meme.funny ) {
            // Estrellas llenas
            Image(
                painter = painterResource(id = R.drawable.ic_star_filled_24),
                contentDescription = "Color star",
                Modifier.clickable { callback( meme.id, it) }
            )
        }
        repeat( 5 - meme.funny) {
            // Estrellas vacías
            Image(
                painter = painterResource(id = R.drawable.ic_star_empty_24),
                contentDescription = "Gray stars",
                Modifier.clickable { callback(meme.id, meme.funny + it) }
            )
        }
    }
}

//@Composable
//@Preview(showSystemUi = true)
//fun Preview() {
//    val memes = listOf(
//        Meme( 1L, "Meme de prueba", "http://algo", 3),
//        Meme( 2L, "Otro Meme de prueba", "http://otroalgo", 1),
//        Meme( 3L, "Otro Meme de prueba", "http://otroalgo", 1),
//        Meme( 4L, "Otro Meme de prueba", "http://otroalgo", 2),
//        Meme( 5L, "Otro Meme de prueba", "http://otroalgo", 1),
//        Meme( 6L, "Otro Meme de prueba", "http://otroalgo", 4),
//        Meme( 7L, "Otro Meme de prueba", "http://otroalgo", 1),
//        Meme( 8L, "Uno más para llenar", "http://algomas", 5),
//    )
//
//    Lista(
//        memes = memes,
//        callback = object : MemeListaInterface {
//        override fun setFunny(wich: Long, howMuch: Int) {}
//    })
//}

//interface MemeListaInterface {
//    fun setFunny(wich: Long, howMuch: Int)
//}