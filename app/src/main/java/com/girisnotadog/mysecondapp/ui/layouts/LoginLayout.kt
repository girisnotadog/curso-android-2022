package com.girisnotadog.mysecondapp.ui.layouts

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.entities.User
import kotlinx.coroutines.flow.StateFlow

@Composable
fun LoginLayout( navController: NavController, userForLogin: StateFlow<Resource<Boolean>>, callbackLogin:(String, String) -> Unit ) {
    var username by rememberSaveable {
        mutableStateOf("")
    }

    var password by rememberSaveable {
        mutableStateOf("")
    }

    var errorMessage by remember {
        mutableStateOf("")
    }

    var showLoading by remember {
        mutableStateOf( false )
    }

    LaunchedEffect(key1 = Unit) {
        userForLogin.collect {
            showLoading = false
            when( it ) {
                is Resource.Error -> errorMessage = "${it.message} ${it.exception?.localizedMessage}"
                is Resource.Loading -> showLoading = true
                is Resource.Success -> navController.navigate("dashboard")
                is Resource.StandBy -> {
//                    username = it.standByData.email
//                    password = it.standByData.password
                }
            }
        }
    }

    Column() {
        Row() {
            if( errorMessage.isNotEmpty() )
                Text(text = "Error: $errorMessage")
        }

        Row() {
            if( showLoading )
                Text(text = "Cargando...")
        }

        Row() {
            Text("Nombre de usuario")
            TextField(value = username, onValueChange = {
                username = it
            })
        }
        Row() {
            Text("Contraseña")
            TextField(value = password, onValueChange = {
                password = it
            })
        }
        Spacer(modifier = Modifier.height( 10.dp ))
        Button(onClick = {
            callbackLogin( username, password )
        }) {
            Text(text = "Iniciar sesión")
        }
    }
}
/**
 * StandBy -> Loading
 * Loading -> Error -> StandBy
 * Loading -> Success
 */
