package com.girisnotadog.mysecondapp.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.database.MyAppDatabase
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.repositories.MemeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class FragmentTwoViewModel(app: Application) : AndroidViewModel( app ) {
    private val appDatabase = MyAppDatabase.getInstance( app.applicationContext )
    private val memesDao = appDatabase.memesDao()
    private val memesRepository = MemeRepository( memesDao )

    private val _memesFlow = MutableStateFlow<Resource<List<Meme>>>( Resource.loading() )
    val memesFlow: StateFlow<Resource<List<Meme>>> = _memesFlow

    fun reload() {
        viewModelScope.launch {
            _memesFlow.value = Resource.loading("Actualizando...")

            try {
                memesRepository.updateMemes()
            } catch (e: Exception) {
                _memesFlow.value = Resource.error("Error al actualizar", e)
            }
        }
    }

    init {
        viewModelScope.launch( Dispatchers.IO ) {
            memesRepository.getMemes().collect {
                _memesFlow.value = it
            }
        }
    }
}