package com.girisnotadog.mysecondapp.ui.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.girisnotadog.mysecondapp.R
import com.girisnotadog.mysecondapp.data.entities.Meme

class RVMemesAdapter(
     var memeList: List<Meme>,
     val clickCallback: (meme: Meme) -> Unit
): RecyclerView.Adapter<RVMemesAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val title: TextView = itemView.findViewById( R.id.fragment_two_rv_memes_item_name )
        private val url: TextView = itemView.findViewById( R.id.fragment_two_rv_memes_item_url )
        private val image: ImageView = itemView.findViewById( R.id.fragment_two_rv_memes_item_image )
        private val layout: ConstraintLayout = itemView.findViewById( R.id.fragment_two_rv_memes_item )

        fun bind(meme: Meme) {
            title.text = meme.name
            url.text = meme.url

            Glide.with( itemView.context ).load( meme.url ).into( image )

            layout.setOnClickListener {
                clickCallback( meme )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from( parent.context ).inflate( R.layout.fragment_two_rv_memes_item, parent, false )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind( memeList.get( position ))
    }

    override fun getItemCount(): Int = memeList.size
}