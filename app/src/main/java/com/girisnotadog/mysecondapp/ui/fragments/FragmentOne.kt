package com.girisnotadog.mysecondapp.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.girisnotadog.mysecondapp.R
import com.girisnotadog.mysecondapp.databinding.FragmentOneBinding

private const val ARG_PARAM1 = "url_avatar"
private const val ARG_PARAM2 = "nombre_avatar"

class FragmentOne : Fragment() {
    private lateinit var binding: FragmentOneBinding

    private var urlAvatar: String? = null
    private var nombreAvatar: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            urlAvatar = it.getString(ARG_PARAM1)
            nombreAvatar = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOneBinding.inflate( inflater, container, false )

        loadData()

        return binding.frameLayout

        //return inflater.inflate(R.layout.fragment_one, container, false)
    }

    fun loadData() {
        binding.fragmentOneAvatarName.text = nombreAvatar

        Glide.with( requireContext() )
            .load( urlAvatar )
            .into( binding.fragmentOneImage )
    }

    companion object {
        @JvmStatic
        fun newInstance(urlAvatar: String, nombreAvatar: String) =
            FragmentOne().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, urlAvatar)
                    putString(ARG_PARAM2, nombreAvatar)
                }
            }
    }


}