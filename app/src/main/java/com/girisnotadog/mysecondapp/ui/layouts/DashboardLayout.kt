package com.girisnotadog.mysecondapp.ui.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.entities.Meme
import com.girisnotadog.mysecondapp.ui.components.Lista
import kotlinx.coroutines.flow.Flow


@Composable
fun DashboardLayout(navController: NavController) {
    Column() {
        Button(onClick = {
            navController.navigate("user")
        }) {
            Text("Ir a User")
        }
        Button(onClick = {
            navController.navigate("memes")
        }) {
            Text("Ir a Memes")
        }
    }
}
