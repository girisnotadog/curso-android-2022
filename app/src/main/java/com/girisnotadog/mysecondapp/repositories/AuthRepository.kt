package com.girisnotadog.mysecondapp.repositories

import android.content.Context
import android.util.Log
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.api.APIClient
import com.girisnotadog.mysecondapp.data.datastore.mySettings
import com.girisnotadog.mysecondapp.data.dto.LoginRequestDto
import kotlinx.coroutines.flow.flow

class AuthRepository(
    val context: Context
) {
    private val TAG = javaClass.simpleName

    fun doLogin( authRequestDto: LoginRequestDto ) = flow<Resource<Boolean>> {
        emit( Resource.loading() )
        try {
            val authResponse = APIClient.authService().doLoginRequest(authRequestDto)

            Log.d(TAG, "$authResponse")
            context.mySettings.updateData {
                it.toBuilder()
                    .setToken( authResponse.data.token )
                    .build()
            }

            emit( Resource.success( true ))
        }catch (e: Exception) {
            emit( Resource.error( "No se pudo hacer login", e ))
        }
    }
}