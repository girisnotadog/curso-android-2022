package com.girisnotadog.mysecondapp.repositories

import android.util.Log
import com.girisnotadog.mysecondapp.data.Resource
import com.girisnotadog.mysecondapp.data.api.APIClient
import com.girisnotadog.mysecondapp.data.database.dao.MemesDao
import com.girisnotadog.mysecondapp.data.entities.Meme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception

class MemeRepository(
    private val memesDao: MemesDao
) {
    private val TAG = javaClass.simpleName

    // OBJETIVO: Obtener la lista de memes del almacenamiento local y si no existen obtenerlos de internet
    //fun getMemes(): Flow<List<Meme>> =
    fun getMemes(): Flow<Resource<List<Meme>>> =
        flow {
            // Al iniciar la petición se emite un recurso de tipo Carando...
            emit(Resource.loading())
                // 1: RECOLECTAMOS LA INFORMACIÓN DE LOCAL
                // Recolectamos todos los elementos de la base de datos y los convertimos en entidades de la aplicación
                val flujoDB: Flow<Resource<List<Meme>>> = memesDao.getAllFlow().map {
                    Resource.success( it.map { it.toEntity() } )
                }.onEach { emit( it ) }

                try {
                    updateMemes()
                }catch (e: Exception) {
                    emit(Resource.error("La operación http falló", e))
                }

                emitAll( flujoDB )
        }

    suspend fun updateMemes() {
        //CoroutineScope(Dispatchers.IO).launch {
            //try {
                // 2: HACEMOS LA PETICIÓN A INTERNET
                val listaDeMemesDeInternet = APIClient.memeService().doGetMemesRequest().data.memes
                Log.d(TAG, "listaDeMemesDeInternet => $listaDeMemesDeInternet")
                // 3: GUARDAMOS LA INFORMACIÓN EN LOCAL
                // 3.1: CUANDO GUARDAMOS EN LA BASE DE DATOS, RECIBIMOS ACTUALIZACIONES EN FLOW

                memesDao.insertAll(
                    listaDeMemesDeInternet.map {
                        it.toDbEntity()
                    }
                )

            //}catch (e: Exception) {
            //    Log.e(TAG, "Error al actualizar")
            //}
        //}
    }
}