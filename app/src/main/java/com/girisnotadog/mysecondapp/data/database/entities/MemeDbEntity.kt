package com.girisnotadog.mysecondapp.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.girisnotadog.mysecondapp.data.entities.Meme

@Entity( tableName = "memes" )
data class MemeDbEntity(
    @PrimaryKey
    val id: Long,
    var name : String,
    var url  : String,
    var funny: Int
) {
    fun toEntity(): Meme =
        Meme( id, name, url, funny)
}
