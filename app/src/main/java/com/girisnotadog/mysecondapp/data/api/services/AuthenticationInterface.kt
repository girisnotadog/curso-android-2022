package com.girisnotadog.mysecondapp.data.api.services

import com.girisnotadog.mysecondapp.data.dto.LoginRequestDto
import com.girisnotadog.mysecondapp.data.dto.LoginResponseDto
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationInterface {

    @POST("api/login")
    suspend fun doLoginRequest(
        @Body request: LoginRequestDto
    ): LoginResponseDto
}