package com.girisnotadog.mysecondapp.data.entities

data class User(
    var name: String,
    var email: String,
    var password: String,
    var age: Int
)
