package com.girisnotadog.mysecondapp.data.datastore

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

object MySettingsDataStoreSerializer: Serializer<MySettingsStore> {
    override val defaultValue: MySettingsStore
        get() = MySettingsStore.getDefaultInstance()
                // Valores predeterminados
            .toBuilder()
            .apply {
                name = "Sin nombre"
                email = "no@tengo.email"
                age = 99
            }
            .build()

    override suspend fun readFrom(input: InputStream): MySettingsStore {
        try {
            return MySettingsStore.parseFrom( input )
        } catch (e: IOException) {
            throw CorruptionException("Se corrompió la información de MySettings")
        }
    }

    override suspend fun writeTo(t: MySettingsStore, output: OutputStream) = t.writeTo( output )
}

