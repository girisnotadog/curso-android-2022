package com.girisnotadog.mysecondapp.data.dto

import com.google.gson.annotations.SerializedName

data class LoginResponseDto(
    val success: Boolean,
    val message: String,
    val data: Data

) {
    data class Data(
        val id: Int,
        val name: String,
        val roles: String,
        val token: String,
        val colegio: String,

        @SerializedName("codigo_colegio")
        val codigoColegio: String
    )
}
