package com.girisnotadog.mysecondapp.data.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import com.girisnotadog.mysecondapp.data.entities.User

val Context.mySettings: DataStore<MySettingsStore> by dataStore(
    fileName = "mySettings.pb",
    serializer = MySettingsDataStoreSerializer
)