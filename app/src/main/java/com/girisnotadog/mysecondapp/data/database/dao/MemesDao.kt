package com.girisnotadog.mysecondapp.data.database.dao

import androidx.room.*
import com.girisnotadog.mysecondapp.data.database.entities.MemeDbEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MemesDao {
    // Nos devuelve un observable: Cuando agrege o reemplace elementos en la tabla, voy a recibir una actualización
    @Query("SELECT * FROM memes")
    fun getAllFlow(): Flow<List<MemeDbEntity>>

    // ==============================================
    @Query("SELECT * FROM memes")
    fun getAll(): List<MemeDbEntity>

    @Query("SELECT * FROM memes WHERE memes.id = :id")
    fun getById(id: Long): MemeDbEntity

    @Delete()
    fun deleteMeme(meme: MemeDbEntity)

    @Insert( onConflict = OnConflictStrategy.REPLACE )
    fun insertAll(memes: List<MemeDbEntity>)
}