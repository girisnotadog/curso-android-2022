package com.girisnotadog.mysecondapp.data.api.interceptors

import com.girisnotadog.mysecondapp.MyApplication
import com.girisnotadog.mysecondapp.data.datastore.mySettings
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response

class AuthTokenInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var token: String = ""

        runBlocking {
            MyApplication.appContext.mySettings.data.first().also {
                token = it.token
            }
        }

        return chain.proceed(
            chain.request()
                .newBuilder()
                .header("Authorization", token)
                .build()
        )
    }
}