package com.girisnotadog.mysecondapp.data.dto

data class MemeServiceReponseDto(
    val success: Boolean,
    val data: Data
) {
    data class Data(
        val memes: List<MemeDto>
    )
}
