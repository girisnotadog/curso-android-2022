package com.girisnotadog.mysecondapp.data.api.services

import com.girisnotadog.mysecondapp.data.dto.MemeServiceReponseDto
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface MemesService {
    @GET("get_memes")
    suspend fun doGetMemesRequest(): MemeServiceReponseDto
}