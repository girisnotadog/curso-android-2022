package com.girisnotadog.mysecondapp.data.api

import com.girisnotadog.mysecondapp.data.api.interceptors.AuthTokenInterceptor
import okhttp3.OkHttpClient
import java.time.Duration

sealed class RetrofitClientStrategy {
    internal val clientBase = OkHttpClient()
        .newBuilder()
        .callTimeout( Duration.ofMillis( 1000 ))
        .readTimeout( Duration.ofMillis( 5000 ))

    abstract val client: OkHttpClient
    abstract val url: String

    class MemeStrategy: RetrofitClientStrategy() {
        override val client: OkHttpClient
            get() = clientBase
                .addInterceptor( AuthTokenInterceptor() )
                .build()

        override val url: String = "https://api.imgflip.com/"
    }

    class AuthStrategy: RetrofitClientStrategy() {
        override val client: OkHttpClient
            get() = clientBase
                .build()

        override val url: String = "https://booktrain.com.mx/"
    }
}