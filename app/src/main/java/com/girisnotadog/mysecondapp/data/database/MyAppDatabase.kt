package com.girisnotadog.mysecondapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import com.girisnotadog.mysecondapp.data.database.dao.MemesDao
import com.girisnotadog.mysecondapp.data.database.entities.MemeDbEntity

@Database(
    entities = [
        MemeDbEntity::class
   ],
    version = 1 // Cada que compilemos una versión con un modelo diferente, hay que incrementar
)
abstract class MyAppDatabase: RoomDatabase() {
    abstract fun memesDao(): MemesDao

    companion object {
        private var INSTANCE: MyAppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): MyAppDatabase {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    MyAppDatabase::class.java,
                    "memes_db"
                )
//                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

            return INSTANCE !!
        }
    }
}