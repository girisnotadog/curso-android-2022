package com.girisnotadog.mysecondapp.data.entities

data class Meme(
    val id   : Long,
    var name : String,
    var url  : String,
    // Que tan divertido es el meme
    var funny: Int // TODO: Convertir en ENUM
)
