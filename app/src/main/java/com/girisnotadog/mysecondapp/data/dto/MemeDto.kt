package com.girisnotadog.mysecondapp.data.dto

import com.girisnotadog.mysecondapp.data.database.entities.MemeDbEntity
import com.girisnotadog.mysecondapp.data.entities.Meme

/**
 * Debe ser igual a la respuesta del servicio WEB
 * DTO = Data Transfer Object
 */
data class MemeDto(
    val id: String, //	"181913649"
    val name: String,//	"Drake Hotline Bling"
    val url: String, //	"https://i.imgflip.com/30b1gx.jpg"
    val width: Int,//	1200
    val height: Int,//	1200
    val box_count: Int //	2
) {
    fun toEntity(): Meme =
        Meme(
            id = id.toLong(),
            name = name,
            url = url,
            funny = 0
        )

    fun toDbEntity(): MemeDbEntity =
        MemeDbEntity(
            id.toLong(),
            name,
            url,
            funny = 0
        )
}
