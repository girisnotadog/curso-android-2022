package com.girisnotadog.mysecondapp.data.api

import com.girisnotadog.mysecondapp.data.api.services.AuthenticationInterface
import com.girisnotadog.mysecondapp.data.api.services.MemesService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIClient {
    private fun serviceBuilder( strategy: RetrofitClientStrategy ) =
        Retrofit.Builder()
            .client( strategy.client )
            .baseUrl( strategy.url )
            .addConverterFactory( GsonConverterFactory.create() )
            .build()

    fun memeService() = serviceBuilder( RetrofitClientStrategy.MemeStrategy() ).create( MemesService::class.java )
    fun authService() = serviceBuilder( RetrofitClientStrategy.AuthStrategy() ).create( AuthenticationInterface::class.java )
}
