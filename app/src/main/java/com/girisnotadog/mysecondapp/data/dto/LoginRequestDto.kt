package com.girisnotadog.mysecondapp.data.dto

data class LoginRequestDto(
    val email: String,
    val password: String
)
